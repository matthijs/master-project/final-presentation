%include talk.fmt
%if style == newcode
\begin{code}
{-# LANGUAGE TemplateHaskell #-}
module Main where

import qualified Prelude as P
\end{code}
%endif

\section{Multiply-Accumulate}
\subsection{Pure description}
\frame
{
\frametitle{Multiply-Accumulate}
Purely Functional Description:
\begin{beamercolorbox}[sep=-2.5ex,rounded=true,shadow=true,vmode]{codebox}
%if style == newcode

%else
\begin{code}
macc (x,y) acc = (u, u)
  where
    u = acc + x * y
\end{code}
\end{beamercolorbox}
}\note[itemize]{
\item Purely functional design (that Jan showed you earlier).
\item \emph{acc} is the current state. \emph{u} is the updated state and output. 
}
%endif

\subsection{Extra's to make it compile}
\frame
{
\frametitle{Extra's to make it compile}
{\clash} Description:
\begin{beamercolorbox}[sep=-2.5ex,rounded=true,shadow=true,vmode]{codebox}
\begin{code}
{-"{\color<3>[rgb]{1,0,0}"-}import CLasH.HardwareTypes{-"}"-}
{-"{\color<4>[rgb]{1,0,0}"-}import CLasH.Translator.Annotations{-"}"-}
{-"{\color<3>[rgb]{1,0,0}"-}type Word = SizedInt D8{-"}"-}

initacc :: {-"{\color<3>[rgb]{1,0,0}"-}Word{-"}"-}
{-"{\color<5>[rgb]{1,0,0}"-}initacc = 0{-"}"-}

ANN(macc (InitState {-"\ "-} `initacc))
ANN(macc TopEntity)
macc :: ({-"{\color<3>[rgb]{1,0,0}"-}Word{-"}"-},{-"{\color<3>[rgb]{1,0,0}"-}Word{-"}"-}) -> {-"{\color<2>[rgb]{1,0,0}"-}State{-"}"-} {-"{\color<3>[rgb]{1,0,0}"-}Word{-"}"-} -> ({-"{\color<2>[rgb]{1,0,0}"-}State{-"}"-} {-"{\color<3>[rgb]{1,0,0}"-}Word{-"}"-}, {-"{\color<3>[rgb]{1,0,0}"-}Word{-"}"-})
macc (x, y) ({-"{\color<2>[rgb]{1,0,0}"-}State{-"}"-} acc) = ({-"{\color<2>[rgb]{1,0,0}"-}State{-"}"-} u, u)
  where
    u = acc + x * y
\end{code}
\end{beamercolorbox}
}\note[itemize]{
\item Stuff you need to add to make it compile in \clash{}.
\item State wrapper, tells the compiler which argument is the state
\item Type declarations. Top-level entity needs to be mono-morphic (and first-order).
\item Annotation pragma's. The TopEntity pragma tells the compiler which entity is the top entity.
\item The InitState pragma points to the initial state. 
}

\begin{frame}[plain]
   \vspace{-0.8em}
   \begin{figure} 
      \centerline{\includegraphics[width=\paperwidth,trim=9mm 4cm 14mm 4cm, clip=true]{macc.png}}
    \end{figure}
\end{frame}
\note[itemize]{
\item We see the register and multiplier on the left
\item The adder on the right
}

\subsection{Test input}
\frame
{
\frametitle{Test input}
Test program for the Multiply-Accumulate circuit:
\begin{beamercolorbox}[sep=-2.5ex,rounded=true,shadow=true,vmode]{codebox}
\begin{code}
ANN(program TestInput)
program :: [(Word,Word)]
program =
  [ (4, 2)  -- 4 * 2 + 0    =   8
  , (1, 3)  -- 1 * 3 + 8    =   11
  , (2, 2)  -- 2 * 2 + 11   =   15
  ]
\end{code}
\end{beamercolorbox}
\begin{itemize}
  \item VHDL Testbench is automatically generated due to the \emph{TestInput} annotation pragma.
\end{itemize}

}\note[itemize]{
\item We can also generate VHDL Testbench
}

%if style == newcode
\begin{code}
run _     _    []        = []
run func state (i:input) = o:out
  where
    (state', o) = func i state
    out         = run func state' input
    
main :: IO ()
main = do
  let input = program
  let istate = (State initacc)
  let output = run macc istate input
  mapM_ (\x -> putStr $ ((show x) P.++ "\n")) output
  return ()
\end{code}
%endif
