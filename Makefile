FILE		= clash-haskell09
LHS2TEX = lhs2TeX -v --tt
LATEXMK = latexmk -pdf
RM			= rm -f
RSVG    = rsvg-convert --format=pdf

LHSRCS = \
	introduction.lhs \
	matthijs/introduction.lhs \
	christiaan/introduction.lhs \
	christiaan/fir.lhs \
	christiaan/dotproduct.lhs \
	christiaan/structure.lhs \
	christiaan/reductioncircuit.lhs \
	christiaan/recursion.lhs

LHFORMATS = \
	talk.fmt
	
TEXSRCS = \
  preamble.tex

SVGFIGURES = \
  mealymachine2.svg \
  mealymachine2-func-red.svg \
  mealymachine2-state-red.svg \
  reducer.svg \
  figures/schakelingen/CMOS_NAND_Layout.svg

default: clash-haskell09

clash-haskell09: texs figs $(TEXSRCS) $(LHFORMATS)
	$(LHS2TEX) $(FILE).lhs > $(FILE).tex; \
	$(LATEXMK) $(FILE); \
	open $(FILE).pdf; \
	$(RM) $(LHSRCS:.lhs=.tex)

texs : $(LHSRCS:.lhs=.tex) 
%.tex : %.lhs
	$(LHS2TEX) $< > $@

figs : $(SVGFIGURES:.svg=.pdf)
%.pdf : %.svg
	$(RSVG) $< > $@

clean:
		latexmk -C clash-haskell09
		$(RM) $(SVGFIGURES:.svg=.pdf)
		$(RM) $(FILE).tex
		$(RM) $(FILE).ptb
		$(RM) $(FILE).synctex.gz
		$(RM) $(FILE).nav
		$(RM) $(FILE).snm
		$(RM) *.hi *.o *.aux
