%include talk.fmt
\section{Presentation Matthijs}
\subsection{Functions}
\frame
{
  \frametitle{Functions}

  \begin{block}{Applying the $sqrt$ function to 4}<1>
  $ sqrt(4) = 2$
  \end{block}
  \begin{block}{A mathematical square function}<2->
  $ f(x) = x * x$
  \end{block}
  
  \begin{block}{A square function in Haskell}<3->
  \texttt{square x = x * x}
  \end{block}
}

\note[itemize]
{
  \item Functionele taal -$>$ Wiskunde-achtig
  \item Functies staan centraal
  \item Next sheet: sqrt example
  \item Van buitenaf:
    \begin{itemize}
      \item Stop er argumenten in (toepassen)
      \item Krijg een resultaat terug
    \end{itemize}
  \item Next sheet: f(x) = x*x example
  \item Van binnenaf:
    \begin{itemize}
      \item \emph{Hoe} werkt de functie (definitie)
    \end{itemize}
  \item Next sheet: square in Haskell
  \item Geen haakjes
}

\frame
{
  \begin{columns}
    \begin{column}{8cm}
      \begin{block}{Pythagoras' theorem}
      $ a^2 + b^2 = c^2 $

      or

      $ c = \sqrt{a^2 + b^2} $
      \end{block}
    \end{column}
    \begin{column}{3cm}
      \includegraphics[width=3cm]{figures/Pythagoras}
    \end{column}
  \end{columns}

  \pause
  % TODO: Alignment is weird due to mixing columns and non-columns
  \begin{block}{Calculating $c$ in Haskell}
    \texttt{pyth a b = sqrt ((square a) + (square b))}
  \end{block}
}

\note[itemize]
{
  \item Functies kunnen door andere functies gebruikt worden
  \item Next sheet: Pyth function
  \item Complexere functie
  \item Next sheet: Pyth in Haskell
}

\frame
{
  \begin{block}{}
    \texttt{pyth a b = sqrt ((square a) + (square b))}
  \end{block}

  \begin{figure}
    \includegraphics[width=9.5cm]{figures/archs/Pyth}
  \end{figure}
}

\note[itemize]
{
  \item Next sheet: Pyth hardware
  \item Argumenten -$>$ input poorten
  \item Toepassing -$>$ component
  \item Resultaat -$>$ output poort
  \item (Combinatorische) hardware is continu
}

\frame
{
  \frametitle{Purity}

  \begin{block}{Pure functions}
    sqrt, pyth
  \end{block}
  \begin{block}{Impure functions}
    random, delete\_file
  \end{block}
}

\note[itemize]
{
  \item Next sheet: Purity
  \item Wiskundige en functionele functies zijn (meestal) puur.
  \item Zelfde argumenten -$>$ zelfde resultaten
  \item Geen bijwerkingen.
}

\subsection{State}

\frame
{
  \frametitle{Multiply-accumulate}
  \begin{columns}
    \begin{column}{4cm}
        \begin{tabular}{lll}
          Input A & Input B & Output \\
          \hline
          1 & 1 & 1 \\
          1 & 2 & 3 \\
          1 & 1 & 4 \\
          2 & 2 & 8 \\
        \end{tabular}
    \end{column}
    \begin{column}{6cm}
      \begin{figure}
        \includegraphics[width=7cm]{figures/archs/Mac}
      \end{figure}
    \end{column}
  \end{columns}
}

\note[itemize]
{
  \item Next sheet: MAC circuit and I/O values
  \item MAC is common circuit
  \item Multiplies pairs, one pair at a time
  \item Stores sum so far
  \item Not pure!
  \item Depends on inputs \emph{and} current register value
  \item Solution: Put register value (state) in argument
}

\frame
{
  \frametitle{Multiply-accumulate}
  \begin{columns}
    \begin{column}{4cm}
      \begin{block}{}
        \vspace{-0.5cm}
\begin{verbatim}
mac (a, b) (State s) = let
  sum = s + (a * b)
in (State sum, sum)
\end{verbatim}
      \end{block}
    \end{column}
    \begin{column}{6cm}
      \begin{figure}
        \includegraphics[width=7cm]{figures/archs/MacExternal}
      \end{figure}
    \end{column}
  \end{columns}
}

\note[itemize]
{
  \item Next sheet: MAC implementation
  \item Current state as argument (annotated)
  \item Two parts in the result: New state and output
  \item Register is placed ``outside''
  \item We want it inside!
  \item Annotation allows compiler to put it inside
}

\begin{frame}[fragile]
  \frametitle{Simulating}

  \begin{block}{Recursive run function}
    run f (i:is) s = let
      (s', o) = f i s
    in o : (run f is s')
  \end{block}

  \begin{block}{Remember \texttt{mac}}
\vspace{-0.5cm}
\begin{verbatim}
mac (a, b) (State s) = let
  sum = s + (a * b)
in (State sum, sum)
\end{verbatim}
  \end{block}
\end{frame}

\note[itemize]
{
  \item Next sheet: run function
  \item Used for simulation only
  \item Recursion ``stores'' state
  \item Each recursion step, \texttt{f} is evaluated once.
}

\subsection{\texorpdfstring{\clash{}}{CLasH}}

\frame{
  \begin{center}
  {\Huge \clash}

  \bigskip
  CAES Language for Synchronous Hardware
  \end{center}
}

\note[itemize]
{
  \item Next sheet: \clash
  \item Hardware beschrijven in Haskell: \clash
  \item Nog niet gezien: keuzes, pattern matching, polymorfisme, hogere orde
  functies....
  \item Dit was de taal -$>$ compiler nodig
}

\frame
{
  \begin{block}{Compiler pipeline}
  $\xrightarrow{Haskell}{GHC frontend}
  \xrightarrow{Core}{Normalization}
  \xrightarrow{Core}{Backend}
  \xrightarrow{VHDL}$
  \end{block}
}

\note[itemize]
{
  \item Next sheet: \clash\ pipeline
  \item GHC: bestaande compiler
  \item Core: simpele functionale taal, niet minder expressief (vergelijk:
  taal zonder synoniemen)
  \item VHDL: Complexe HDL, gebruiken alleen netlist onderdelen.
  \item Normalisatie: Nog simpeler maken (Polymorphisme, hogere orde eruit,
  eenduidige vorm).
}

\subsection{Normalization}
\frame
{
  \frametitle{Netlists}

  \bigskip
  \bigskip
  \begin{figure}
    \includegraphics[width=9.5cm]{figures/archs/Square}
  \end{figure}

  \begin{block}{Remember \texttt{square}}
    square x = x * x
  \end{block}
 
}

\note[itemize]
{
  \item Next sheet: Square netlist
  \item Core hetzelfde als Haskell -$>$ gebruik Haskell syntax voor Core
  \item Netlist is componenten (vierkantjes) en verbindingen (lijntjes)
  \item Component heeft poorten, poorten moeten een naam hebben
  \item Resultaat van square heeft geen naam -$>$ geen normaalvorm
}

\frame
{
  \frametitle{Transformation}
  \begin{columns}
    \begin{column}{5cm}
      func = E
      
      \smallskip
      \hrule

      \smallskip
      func = let res = E in res
    \end{column}
    \begin{column}{5cm}
      E has no name
    \end{column}
  \end{columns}

  \pause
  \bigskip
  \begin{block}{Apply to square}
    square x = x * x

    \smallskip
    \hrule

    \smallskip
    square x = let res = x * x in res
  \end{block}
}

\note[itemize]
{
  \item Next sheet: transformation example
  \item Transformatie nodig om output poort een naam te geven
  \item Next sheet: Apply to square
  \item Toepassen op square, res heeft nu een naam.
}

\frame
{
  \frametitle{Normalization system}
  
  \begin{figure}
    \includegraphics[width=9.5cm]{figures/norm/Normalizing}
  \end{figure}
}

\note[itemize]
{
  \item Systeem van transformaties
  \item Volgorde ongedefinieerd: Doe maar wat, tot het net meer kan.
  \item Next sheet: Graafnotatie
  \item Lijnen zijn transformaties
  \item Keuzes (meerdere uitgaande lijnen)
  \item Alles komt onderin uit
}

\frame
{
  \frametitle{Normalization system}
  
  \begin{figure}
    \includegraphics[width=8cm]{figures/norm/Incorrect}
  \end{figure}
}

\note[itemize]
{
  \item Meerdere normaalvormen
  \item Cykel
  \item Systeem werkt nu \emph{meestal}
  \item verbeteringen en bewijzen nodig
}

\frame
{
  \frametitle{Normalization system}

  \begin{itemize}
    \item Easy to work with
    \item Allows analysis
    \item Still complex
  \end{itemize}
}

\note[itemize]
{
  \item Goede scheiding
  \item Makkelijk implementeren
  \item Wiskundige beschrijving -$>$ bewijzen
  \item Huidige systeem is nog incompleet
  \item De goede weg
}

\subsection{Summary}

\frame
{
  \frametitle{But now?}
  \begin{itemize}
    \item \clash\ has a solid base
    \item Lots of work left
    \item Needs testing!
  \end{itemize}
}

\note[itemize]
{
  \item \clash\ is helemaal nieuw werk
  \item Tekortkomingen: Haskell is niet ideaal, state beschrijvingen zijn
  niet compact, transformaties zijn nog niet (bewezen compleet).
  \item Meer testen nodig, 
  \item Met meer testen en meer werk -$>$ taal van de toekomst!
}

\subsection{Thanks}
\frame
{
\vspace{2cm}\centerline{\Huge{Thanks!}}
}
    
% vim: set filetype=tex sw=2 sts=2 expandtab:
