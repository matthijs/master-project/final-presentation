\subsection{Restrictions}
%include talk.fmt
\frame{
\frametitle{Too Restrictive?}
\begin{itemize}
  \item Is \clash{} too restrictive given the fact that a designer can currently not define his own vector transformations, or recursive functions for that matter?
\end{itemize}
}
\note[itemize]{
\item Je kan je natuurlijk afvragen of er nu niet teveel restricties zijn binnen \clash{}. Dit valt op zich wel mee, gezien veel gebruikte lijst-functies, zoals: map, zipwith, fold, enzo wel beschikbaar zijn
\item next sheet: expressief genoeg voor reductiecircuit
}

\frame{
\frametitle{Too Restrictive?}
\begin{itemize}
  \item There is certainly room to increase expressivity. But we can already describe non-trivial design in \clash{}.
  \item Example: Reduction circuit
\end{itemize}
}
\note[itemize]{
\item Recursieve functies zijn soms wel een gemis, en er is ook zeker ruimte om de expressiviteit van \clash{} uit te breiden. Maar we kunnen al wel niet-triviale ontwerpen maken, zoals het reductiecircuit wat ik nu ga laten zien.
\item Next sheet: beschrijving reductie circuit
}

\subsection{Reduction circuit}

\frame{
\frametitle{Reduction Circuit}
\begin{columns}[l]
\column{0.5\textwidth}
\begin{figure}
\includegraphics[height=6.5cm]{reducer}
\end{figure}
\column{0.5\textwidth}
\begin{itemize}
  \item Reduction circuit sums the floating-point values of each row in a matrix.
  \item Non-trivial due to pipe-lined floating-point adder.
  \item Largest restrictions are the fixed-size vectors.
\end{itemize}
\end{columns}
}
\note[itemize]{
\item Reductiecircuit telt voor alle rijen, de getallen uit zo'n rij bij elkaar op.
\item Het probleem is niet triviaal, omdat resultaten met een bepaalde vertraging uit de opteller komen vanwege pipelining. Triviale oplossing zoals een simpel accumulate register zijn niet mogelijk omdat dan per ongelijk meerdere rijen bij elkaar opgeteld worden. Ook kan je niet wachten op het resultaat, omdat je mogelijk een oneindig grote input buffer nodig hebt.
\item Daarom heb je speciale controle logica nodig die bijhoud welke waarden er in de opteller zitten.
\item De grootste beperking in het ontwerp is niet het gebrek aan recursieve functies, maar het gebrek aan dynamishe lijsten.
\item next sheet: synthese output reductie circuit
}

\begin{frame}
   \begin{figure} 
      \includegraphics[height=9cm]{reducerschematic} 
    \end{figure}
\end{frame}
\note[itemize]{
\item Zie hier het werkelijke circuit
\item Next sheet: fifo buffer wens
}

\begin{frame}
\frametitle{FIFO Buffer}
\begin{itemize}
  \item Wish:
\begin{code} 
fifo :: (State mem) (input, shift) = 
  (State mem', out1, out2)
  where
    out1 | length mem == 0 = NotValid
         | otherwise       = head mem
    out2 | length mem < 2  = NotValid
         | otherwise       = head (tail mem)
    {-"{\color<2>[rgb]{1,0,0}"-}mem' = drop shift mem ++ [input]{-"}"-}
\end{code}
\end{itemize}
\end{frame}
\note[itemize]{
\item Hier het gewenste ontwerp van de input (FIFO) buffer.
\item Zie vooral de laatste regel. De lengte van mem' is afhankelijk van de waarde van shift. Dus dynamish.
\item Next sheet: fifo buffer realiteit
}

\begin{frame}
\frametitle{FIFO Buffer}
\begin{itemize}
  \item Reality:
\begin{code} 
fifo :: (State (Fifo {..})) (inp, shift) = 
  ( State (Fifo { mem = mem'
                , ptr = ptr'     
                })
  , out1, out2
  )
  where
    {-"{\color<2>[rgb]{1,0,0}"-}ptr'  = ptr - shift + 1{-"}"-}
    {-"{\color<2>[rgb]{1,0,0}"-}mem'' = replace mem ptr (Valid inp){-"}"-}
    {-"{\color<2>[rgb]{1,0,0}"-}mem'  | shift == 0 = mem''{-"}"-}
    {-"{\color<2>[rgb]{1,0,0}"-}      | shift == 1 = (tail mem'') <+ NotValid{-"}"-}
    {-"{\color<2>[rgb]{1,0,0}"-}      | otherwise  = ((tail (tail mem''){-"}"-} 
    {-"{\color<2>[rgb]{1,0,0}"-}                      <+ NotValid) <+ NotValid){-"}"-}
    out1  = head mem
    out2  = head (tail mem) 
\end{code}
\end{itemize}
\end{frame}
\note[itemize]{
\item Die dynamishe lijsten hebben we dus niet in \clash{}
\item Daarom moeten we heel veel logica toevoegen om iets te maken wat het gedrag van een dynamishe lijst emuleert.
\item Next sheet: dynamish vs statisch
}

\frame{
\frametitle{FIFO Buffer}
\begin{itemize}
  \item Wish: Dynamically sized vectors
  \item Reality: Statically sized vectors
\end{itemize}
}
\note[itemize]{
\item Dus, de wens is dynamishe lijsten, en de realiteit is statische lijsten. Dit omdat statische lijsten veel makelijker naar hardware zijn te mappen. Ze zijn gewoon 1-op-1 te kopieren.
\item Dynamische lijsten moeilijk, ik kan niet op run-time extra hardware maken. Wat bij software wel gebeurd: gewoon meer RAM alloceren
\item Next sheet: problemen/mogelijkheden dynamische lijsten
}

\frame{
\frametitle{Dynamically Sized Vectors}
\begin{itemize}
  \item Map all vectors to RAMs:
  \begin{itemize}
    \item Store length separately, extra logic
    \item What happens if size of the vector exceeds size of the size of the RAM?
  \end{itemize}
  \item Translate to (shift/circular) Buffers
  \begin{itemize}
  \item Requires analysis of data-access
  \item How do we determine maximum size?
  \end{itemize}
\end{itemize}
}
\note[itemize]{
\item Wat zijn dan mogelijkheden voor dynamische lijsten?
\item Alle lijsten naar RAMs mappen... maar dan is er wel extra logica nodig om de lengte bij te houden. Wat gebeurd er als de RAM eenheid vol is?
\item We kunnen ook proberen om de operaties te vertalen naar shift/circulaire buffers... Maar dan moeten we dus wel data-access analyse doen... en we moeten de maximale lengte kunnen bepalen...
\item Allemaal niet triviaal dus
\item Next sheet: belangrijkste future work, recursie
}