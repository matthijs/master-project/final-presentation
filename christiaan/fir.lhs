%include talk.fmt
\subsection{FIR Filter}

\begin{frame}
   \frametitle{Example: FIR Filter}
   \begin{itemize}
     \item FIR Filters, and digital filters in general are essential components in radio's, receivers, and cellphones.
     \item Equation for a FIR Filter: \\
     \[
     y_t  = \sum\nolimits_{i = 0}^{n - 1} {x_{t - i}  \cdot h_i } 
     \]
   \end{itemize}
\end{frame}
\note[itemize]{
\item FIR filter is een digitaal filter.
\item Filters worden gebruik om bepaalde eigenschappen van een radiosignaal te versterken of te verzwakken.
\item Filters zijn een belangrijk onderdeel in mobiele telefonie
\item Next Sheet: Beschrijving formule
}

\begin{frame}
   \frametitle{FIR Filter}
     \[
     y_t  = \sum\nolimits_{i = 0}^{n - 1} {x_{t - i}  \cdot h_i } 
     \]
\end{frame}
\note[itemize]{
\item De formule zegt niks meer dan:
\item Vermenigvuldig het signaal van meerdere tijdsmomenten (X\_t t/m X\_(t-n)) met bepaalde (moglijk negatief) getallen (H\_i), en tel deze waarden bij elkaar op.
\item Next Sheet: Dot-product
}

\input{christiaan/dotproduct}

\begin{frame}
   \frametitle{FIR Filter}
     \[
     y_t  = \sum\nolimits_{i = 0}^{n - 1} {x_{t - i}  \cdot h_i } 
     \] \\
     \begin{code}
       fir (State {-"{\color<2>[rgb]{1,0,0}"-}pxs{-"}"-}) {-"{\color<3>[rgb]{1,0,0}"-}x{-"}"-} = ({-"{\color<5>[rgb]{1,0,0}"-}pxs**hs{-"}"-}, State ({-"{\color<4>[rgb]{1,0,0}"-}pxs<++x{-"}"-}))
         where hs = $(vectorTH [2::Int16,3,-2,4])
     \end{code}
     \centerline{\begin{tabular}{rl}
     {\color<2>[rgb]{1,0,0}|pxs|}  & Previous x's (state)\\
     {\color<3>[rgb]{1,0,0}|x|} & New input value\\
     {\color<4>[rgb]{1,0,0}|pxs <++ x|} & Remember new |x|, remove oldest\\
     {\color<5>[rgb]{1,0,0}|pxs ** hs|} & Output
     \end{tabular}}
\end{frame}
\note[itemize]{
\item Zie hier dus de code voor het FIR filter
\item Er zijn zoals je ziet 4 getallen, coefficienten, die vermenigvuldigt moeten worden.
\item In |pxs| komen dus 4 waarden van X te staan.
\item de |<++| operatie schuift er steeds een nieuwe X in, en gooit de oudste er uit.
\item Je zie ook het inproduct, |**|, terug
\item Next sheet: demo
}

\begin{frame}
   \frametitle{FIR Filter}
   \centerline{\Huge{Demo}}
\end{frame}
\note[itemize]{
\item Code laten zien
\item Code compilen
\item Next sheet: synthese output
}

\begin{frame}
   \frametitle{Synthesized Output}
   \vspace{-0.8em}
   \begin{figure} 
      \centerline{
\includegraphics<1>[width=\paperwidth,trim=9mm 14cm 14mm 16cm, clip=true]{fir0.png}
\includegraphics<2>[width=\paperwidth,trim=9mm 15cm 16.5cm 11cm, clip=true]{fir1.png}
\includegraphics<3>[width=\paperwidth,trim=3cm 13cm 4cm 11cm, clip=true]{fir2.png}}
    \end{figure}
\end{frame}
\note[itemize]{
\item Overzicht van de hardware
\item De 4 geheugen elementen
\item Aan de linkerkant de 4 vermenigvuldiger, rechts de 3 optellers
\item Next Sheet: Structuur / grootte afleiden
}