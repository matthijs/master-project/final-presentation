\subsection{Recursion}
%include talk.fmt
\frame{
\frametitle{Future Work: Recursion}
\begin{itemize}
  \item The most pressing future work is to support recursive functions.
  \item Partially explored solution: static loop unrolling
  \item Unexplored solution: Haskell language extensions, new source language
\end{itemize}
}
\note[itemize]{
\item Afsluitend nog het meest voor de hand liggende future work: het ondersteunen van recursie.
\item Er zijn al wat mogelijkheden onderzocht: static loop unrolling, waar ik zo wat over vertel.
\item Andere mogelijkheden zijn het op zoek gaan naar een taal die wel echt ondersteuning heeft voor dependent types. Daar later meer over.
\item Next sheet: static loop unrolling
}

\subsubsection{Static loop unrolling}
\frame{
\frametitle{Static loop unrolling}
\begin{itemize}
  \item Unroll, and simplify recursive definitions at compile time.
  \item Explored solution: Unrolling number-guided recursive functions using Template Haskell
\end{itemize}
}
\note[itemize]{
\item Static loop unrolling, is het op compile-time uitrollen van recursieve functies totdat er geen recursie meer is. Je moet de functies ook simplificeren omdat je de choice-logic die de recursie leidt wil verwijderen.
\item Ik heb gekeken naar het uitrollen van number-guided recursive functies, met gehulp van Template Haskell.
\item Next sheet: Template Haskell uitleg
}

\frame{
\frametitle{Template Haskell}
\begin{itemize}
  \item Template Haskell allows for compile-time inspection, construction, and manipulation of Haskell code.
  \item All these functions are expressible in normal Haskell
\end{itemize}
}
\note[itemize]{
\item Ik heb vooral naar Template Haskell gekeken, omdat toekomstig gebruikes van \clash{} ook beschikking hebben om TH te kunnen gebruiken. Zonder dat ze direct de compiler in moeten duiken.
\item TH staat het toe om code op compile-time te inspecteren, te creeeren en te veranderen.
\item En dit kan allemaal met behulp van normale Haskell code
\item Next sheet: voorbeeld uitrollen tree adder
}

\frame{
\frametitle{Tree Adder}
\begin{code}
$(do
  [typ, _] <- [d|{
{-"{\color<2>[rgb]{1,0,0}"-}treeSum :: Vector D8 (SizedWord D8) -> SizedWord D8;{-"}"-}
treeSum xs = undefined
  }|]
  [func] <- [d|{
{-"{\color<3>[rgb]{1,0,0}"-}treeSum i xs | i < 1     = head xs{-"}"-}
{-"{\color<3>[rgb]{1,0,0}"-}             | otherwise = let (a,b) = split xs{-"}"-}
{-"{\color<3>[rgb]{1,0,0}"-}                           in (treeSum (i-1) a) +{-"}"-} 
{-"{\color<3>[rgb]{1,0,0}"-}                              (treeSum (i-1) b){-"}"-}
  }|]
{-"{\color<4>[rgb]{1,0,0}"-}  let func' = unroll Nothing 0 (IntegerL 3) func{-"}"-}
{-"{\color<5>[rgb]{1,0,0}"-}  return [typ,func']{-"}"-}
)
\end{code}
}
\note[itemize]{
\item Zie hier een recursieve tree adder die ik uitroll naar 3 diep d.m.v. template haskell
\item Zie de function signature bovenaan.
\item Dan de de recursieve treeSum functie
\item Dan de unroll functie die deze treeSum functie uitrolt
\item En dan voegen we de uitegerolde functie weer in
\item Next sheet: schema uitgerolde tree adder
}

\begin{frame}
   \frametitle{Unrolled tree adder}
   \begin{figure} 
      \includegraphics[height=5cm]{treeadder} 
    \end{figure}
\end{frame}
\note[itemize]{
\item Hier een schema van uitgerolde tree-adder voor 3 diep
\item Volgende sheet: Aanpassingen input taal voor \clash{}
}

\subsubsection{Input Language}
\frame{
\frametitle{Input Language}
\begin{itemize}
  \item New source language: One of the problems is that Haskell does not properly support dependent types. Investigate languages with dependent type systems.
  \item Haskell extentions: Invariant descriptions at the type-level.
\end{itemize}
}
\note[itemize]{
\item Ondersteuning van dependent types is niet echt bestaand in Haskell. Is meer op een ad-hoc manier mogelijk, maar is bij ontwerp van het type-systeem nooit de bedoeling geweest.
\item Daarom zou er gekeken moeten worden naar een echte dependently typed taal. Die het hopelijk makkelijker maakt om invarianten te specificeren, en is toe te voegen aan de code.
\item Of er moet worden gekeken of er in Haskell de mogelijkheid bestaat om invarianten op type-niveau toe te voegen.
\item Next sheet: vragen
}