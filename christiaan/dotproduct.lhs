%include talk.fmt
\subsubsection{Dot Product}
\begin{frame}
   \frametitle{Dot Product}
     \[
     y = \overrightarrow x  \bullet \overrightarrow h 
     \]
\end{frame}
\note[itemize]{
\item FIR filter doet niks meer dat inproduct toepassen op een aantal waardes.
\item Next sheet: uitleg dot product
}

\begin{frame}
   \frametitle{Dot Product}
     \[
     y = \overrightarrow x  \bullet \overrightarrow h 
     \]
     \[
     \overrightarrow x  \bullet \overrightarrow h  = {a_1  \cdot b_1  + a_2  \cdot b_2  +  \ldots  + a_n  \cdot b_n } 
     \]
\end{frame}
\note[itemize]{
\item Inproduct is niks anders dan het paarsgewijs vermenigvuldigen en dan optellen van 2 lijsten.
\item Next sheet: de operaties die same dotproduct vormen
}

\begin{frame}
  \frametitle{Dot Product}
   \begin{itemize}
    \item Two steps to define: \\
    \[
    \overrightarrow x  \bullet \overrightarrow h  = {a_1  \cdot b_1  + a_2  \cdot b_2  +  \ldots  + a_n  \cdot b_n } 
    \]
    \begin{itemize}
      \item \emph{Pairwise Multiplication}: \\
      \begin{code}
      zipwith (*) xs hs =
        < x0*h0, x1*h1, x(n-1)*h(n-1)>       
      \end{code}
      \item \emph{Summation}: \\
      \begin{code}
      foldl (+) 0 zs =
        (..((0+z0)+z1)+..+z(n-1))
      \end{code}
    \end{itemize}
   \end{itemize}
\end{frame}
\note[itemize]{
\item Paarsgewijs vermenigvuldigen gebeurd met zipWith functie. zipWith voegt twee lijsten samen door op de elementen uit de lijst en functie toe te passen. In dit voorbeeld dus vermenigvuldiging.
\item Foldl vouwt een lijst op door een functie herhaaldelijk op elk opvolgend element toe te passen, en zo dus een lijst te reduceren tot een enkele waarde. Nu dus dmv optelling.
\item Next sheet: samenvoegen tot 1 functie
}

\begin{frame}
  \frametitle{Dot Product}
   \begin{itemize}
    \item Two steps to define: \\
    \[
    \overrightarrow x  \bullet \overrightarrow h  = {a_1  \cdot b_1  + a_2  \cdot b_2  +  \ldots  + a_n  \cdot b_n } 
    \]
    \begin{itemize}
      \item \emph{Combine the two}: \\
      \begin{code}
   xs ** hs = foldl (+) 0 (zipWith (*) xs hs)
      \end{code}
    \end{itemize}
   \end{itemize}
\end{frame}
\note[itemize]{
\item Samen vormen deze operaties dus het inproduct.
\item Next sheet: code FIR filter
}

\begin{frame}
  \frametitle{Dot Product}

  \includegraphics[height=\paperheight]{figures/archs/Dotproduct}
\end{frame}

\note[itemize]{
\item Architecture for a dot product with length 4
}
