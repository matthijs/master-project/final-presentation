%include talk.fmt
\title{\clash :}
\subtitle{From Haskell To Hardware}
\author{Christiaan Baaij}
\date{December 14, 2009}

\section{Presentation Christiaan}
\frame{\titlepage \setcounter{framenumber}{1}}
\note[itemize]{
\item Beginnen met een voorbeeld
\item Van een wiskundige beschrijving naar hardware
\item Next Sheet: FIR
}

\input{christiaan/fir}
\input{christiaan/structure}
\input{christiaan/reductioncircuit}
\input{christiaan/recursion}

\subsection{Questions}
\frame{\vspace{2cm}\centerline{\Huge{Questions?}}}