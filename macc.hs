{-#  LANGUAGE TemplateHaskell, TypeOperators, RecordWildCards, ScopedTypeVariables, TypeFamilies  #-}
module MultiplyAccumulate where

import CLasH.HardwareTypes
import CLasH.Translator.Annotations
type Word = SizedInt D8

initacc :: Word
initacc = 0

{-# ANN macc (InitState  'initacc) #-}
{-# ANN macc TopEntity #-}
macc :: (Word,Word) -> State Word -> (State Word, Word)
macc (x, y) (State acc) = (State u, u)
  where
    u = acc + x * y

{-# ANN program TestInput #-}
program :: [(Word,Word)]
program =
  [ (4, 2) --  4 * 2 + 0  = 8
  , (1, 3) --  1 * 3 + 8  = 11
  , (2, 2) --  2 * 2 + 11 = 15
  ]

run _     _    []        = []
run func state (i:input) = o:out
  where
    (state', o) = func i state
    out         = run func state' input
