%include talk.fmt
\section{Introduction}
\subsection{Hardware as we know it}
\frame
{
\frametitle{Hardware}
\begin{figure}
\centerline{
\includegraphics<1>[height=8cm]{figures/cpus/pmiphone_boardtopbig}
\includegraphics<2>[height=8cm]{figures/cpus/Intel_core_i7}
\includegraphics<3>[height=8cm]{figures/cpus/6600GT_GPU}
% \includegraphics<4>[height=8cm]{figures/cpus/Altera_StratixIV}
}
\label{img:chips}
\end{figure}
}
\note[itemize]
{
\item Voorbeelden van hardware / chips
\item Next sheet: Binnenkant iPhone met veelvoud aan chips
\item Next sheet: Laatste Intel CPU
\item Next sheet: Nvidia Videochip
\item Next sheet: Altera FPGA
}

\frame
{
\frametitle{Designing Hardware}
\centerline{Design with 4 transistors}
\begin{columns}[c]
\column{0.6\textwidth}
\vspace{-0.5cm}
\begin{figure}
\centerline{\includegraphics[height=6cm, trim = 0 0 0 2.5cm, clip]{figures/schakelingen/NAND}}
\end{figure}
\column{0.4\textwidth}
\begin{figure}
\centerline{\includegraphics[height=6cm, trim = 0 4.5cm 0 0, clip]{figures/schakelingen/CMOS_NAND_Layout}}
\end{figure}
\end{columns}
}
\note[itemize]
{
\item Next sheet: Transistor NAND design
\item Met de hand ontwerpen
\item Basis zijn transistoren
\item Links een schematisch ontwerp, Rechts de layout van de metaal lagen
}

\frame
{
\frametitle{Transistors}
\begin{figure}
\centerline{
\includegraphics<1>[height=8cm]{figures/transistor/hr-1sttransistor}
\includegraphics<2>[height=8cm]{figures/transistor/worldsfastes}
\includegraphics<3>[height=8cm]{figures/transistor/nehalem_432x315}
}
\label{img:chips}
\end{figure}
}
\note[itemize]
{
\item Next sheet: Eerste transistor 60 jaar geleden
\item Next sheet: Superkleine transistoren
\item Next sheet: 731 miljoen transistoren in deze chip
}

\frame
{
\frametitle{Designing Hardware}
\vspace{0.5cm}
\centerline{\Large Drawing won't work for 730 million transistors!}
\begin{figure}
\centerline{\includegraphics[height=7cm]{figures/transistor/nehalem-core}}
\end{figure}
}

\note[itemize]
{
\item Next sheet: Floorplan 730 million
\item Computer details laten uitzoeken.
}

\frame
{
\frametitle{Designing Hardware}
\begin{itemize}
\item We design hardware in the same format as software
\item We make designs in plain text!
\item Software: Programming Languages
\item Hardware: Hardware Description Languages
\end{itemize}
}
\note[itemize]
{ 
\item Hardware designs worden gewoon in tekst opgeschreven.
\item Maar wel in een special taal voor hardware
\item Waar je voor software programmeer talen hebt,
\item Heb je voor hardware zogeheette hardwarebeschrijvingstalen
}

\frame
{
\frametitle{Designing Hardware}
\begin{columns}
\begin{column}{6cm}
\begin{itemize}
\item Behavioral Descriptions:
\begin{itemize}
\item \emph{What} the hardware does
\end{itemize}
\item Structural Descriptions:
\begin{itemize}
\item \emph{How} the hardware does it
\end{itemize}
\end{itemize}
\end{column}
\begin{column}{4cm}
\includegraphics[width=4cm]{figures/Gradient}
\end{column}
\end{columns}
}
\note[itemize]
{
\item Next sheet: Behavioral vs structural
\item Wat moet de hardware doen? Wiskundig, algoritmisch.
\item Hoe moet de hardware het doen? Compositie, structuur.
\item Balans, meestal beetje van beiden.
\item Graag zo veel mogelijk behavioral (makkelijker)
}

\frame
{
\frametitle{Why do we make Hardware?}
\begin{itemize}
\item We make hardware to solve problems
\item Solutions (Algorithms) often described as a set of mathematical equations
\item `Holy Grail' Hardware Descriptions:
\begin{itemize}
\item Input: Set of Mathematical Equations
\item Output: Hardware that is Provably correct, Fast , Small, Cheap to manufacture, and has a low Energy Usage
\end{itemize}
\end{itemize}
}
\note[itemize]
{
\item Hardware lost problemen op (videodecodering, telefoongesprekken versturen, etc.)
\item Oplossingen zijn beschreven vaak wiskundige formules
\item Heilige graal -$>$ alles automatisch
\item Onbereikbaar
\item Hebben we geen wiskundige programmeertalen?
}

\frame
{
\frametitle{Functional Languages}
\begin{itemize}
\item Functionele talen liggen dicht bij de wiskunde
\item Calculate $2 * 3 + 3 * 4$
\begin{itemize}
\item First calculate $2* 3$ ?
\item Or start with $3 * 4$ ?
\end{itemize}
\end{itemize}
}

\note[itemize]
{
\item Functionele talen eisen (vaak) geen volgorde van uitrekenen
\item Hardware ook niet
\item Lijkt goed overeen te komen
}

% \frame
% {
% \frametitle{Mealy Machine}
% \begin{figure}
% \centerline{\includegraphics<1>[width=6.25cm]{mealymachine2}
% \includegraphics<2>[width=6.25cm]{mealymachine2-func-red}
% \includegraphics<3>[width=6.25cm]{mealymachine2-state-red}}
% \label{img:mealymachine}
% \end{figure}
% \begin{beamercolorbox}[sep=-2.5ex,rounded=true,shadow=true,vmode]{codebox}
% \begin{code}
% run {-"{\color<2>[rgb]{1,0,0}"-}func{-"}"-} {-"{\color<3>[rgb]{1,0,0}"-}state{-"}"-} [] = []
% run {-"{\color<2>[rgb]{1,0,0}"-}func{-"}"-} {-"{\color<3>[rgb]{1,0,0}"-}state{-"}"-} (i:inputs) = o:outputs
%   where
%     ({-"{\color<3>[rgb]{1,0,0}"-}state'{-"}"-}, o)  =   {-"{\color<2>[rgb]{1,0,0}"-}func{-"}"-} i {-"{\color<3>[rgb]{1,0,0}"-}state{-"}"-}
%     outputs                                         =   run {-"{\color<2>[rgb]{1,0,0}"-}func{-"}"-} {-"{\color<3>[rgb]{1,0,0}"-}state'{-"}"-} inputs
% \end{code}
% \end{beamercolorbox}
% }
% \note[itemize]{
% \item A Mealy machine bases its output on the current state and the input
% \item State is part of the function signature
% \item Both the current state, and the updated State
% \item The run function simulates a mealy machine for the provided number of inputs
% }
% 
% \frame
% {
% \frametitle{Haskell Description}
% \begin{figure}
% \centerline{\includegraphics[width=6.25cm]{mealymachine2-func-red}}
% \end{figure}
% \begin{beamercolorbox}[sep=-2.5ex,rounded=true,shadow=true,vmode]{codebox}
% \begin{code}
% func :: 
%   InputSignal ->
%   State ->
%   (State, OutputSignal)
% \end{code}
% \end{beamercolorbox}
% }
% \note[itemize]{
% \item In \clash{} you describe the logic part of the mealy machine
% \item The state in the signature is turned into memory elements when translating to VHDL
% }
