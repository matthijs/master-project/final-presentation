\section{4-taps FIR Filter}

\begin{frame}[plain]
   \vspace{-0.8em}
   \begin{figure} 
      \centerline{\includegraphics[width=\paperwidth,trim=9mm 4cm 14mm 4cm, clip=true]{fir0.png}}
    \end{figure}
\end{frame}

\begin{frame}[plain]
   \vspace{-0.8em}
   \begin{figure} 
      \centerline{\includegraphics[width=\paperwidth,trim=9mm 4cm 14mm 4cm, clip=true]{fir1.png}}
    \end{figure}
\end{frame}

\begin{frame}[plain]
   \vspace{-0.8em}
   \begin{figure} 
      \centerline{\includegraphics[width=\paperwidth,trim=9mm 4cm 14mm 4cm, clip=true]{fir2.png}}
    \end{figure}
\end{frame}