\section{Real Hardware Designs}
\frame{
\frametitle{Beyond trivial designs}
\pause
\begin{columns}[l]
\column{0.5\textwidth}
\vspace{-2.4em}
\begin{figure}
\includegraphics<2->[height=6.5cm]{reducer}
\end{figure}
\column{0.5\textwidth}
\begin{itemize}
  \item We implemented a reduction circuit in \clash{}\pause
  \item Haskell and VHDL simulation match\pause
  \item Synthesis completes without errors or warnings\pause
  \item Around half the speed of handcoded and optimized VHDL
\end{itemize}
\end{columns}
}\note[itemize]{
\item Toys like the poly cpu one are good to give a quick demo
\item But we used \clash{} to design 'real' hardware
\item Reduction circuit sums the numbers in a row, of different length
\item It uses a pipelined adder: multiple rows in pipeline, rows longer than pipeline
\item We hope you see this is not a trivial problem
\item Nice speed considering we don't optimize for it (only single example!)
}

\begin{frame}[plain]
   \vspace{-0.8em}
   \begin{figure} 
      \includegraphics[height=\paperheight]{reducerschematic} 
    \end{figure}
\end{frame}
\note[itemize]{
\item Big part on the left is the input buffer
\item Big part on the right is the buffer for partially reduced results
\item In the middle is the control logic and the adder pipeline
}
