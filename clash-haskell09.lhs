\RequirePackage{atbegshi}
\documentclass[empty]{beamer}
\usetheme{default}
%\setbeameroption{show only notes}
\setbeameroption{show notes}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}
{
  \hbox{%
  \centerline{\insertframenumber{}}}%
  \vskip5pt%
}

\addtobeamertemplate{note page}{
  \vskip5pt
  \centerline{\insertframenumber}
  \vskip5pt
}{}

%include talk.fmt
\include{preamble}

\title{Haskell as a higher order structural hardware description language}
\author{Matthijs Kooijman}
\date{December 14, 2009}

\begin{document}

\frame{\titlepage}
\note[itemize]{
\item De taal \clash{}
\item De compiler voor \clash{}
\item Eerst hardware introductie (gezamenlijk)
}

\include{introduction}
\include{matthijs/introduction}
\include{christiaan/introduction}
% \include{fir}
% \include{reducer}

\end{document}
% vim: set filetype=tex sw=2 sts=2 expandtab:
